/**
 * Feel free to explore, or check out the full documentation
 * https://docs.newrelic.com/docs/synthetics/new-relic-synthetics/scripting-monitors/writing-scripted-browsers
 * for details.
 *
 **/
 const Q = require('q')
 const assert = require('assert')
 const https = require('https')
 const request = require('request')
 
 const insertkey = '' // Inserta API Key
 const accountId = ''  // Inserta su cuenta
 
 const targets = [
   'google.com'
 ]
 
 const currentDate = new Date()
 
 function treatAsUTC (date) {
   const result = new Date(date)
   result.setMinutes(result.getMinutes() - result.getTimezoneOffset())
   return result
 }
 
 function daysBetween (startDate, endDate) {
   const millisecondsPerDay = 24 * 60 * 60 * 1000
   return Math.round((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay)
 }
 
 Q.spawn(function * () {
   for (const index in targets) {
     const target = targets[index]
     call(target)
   }
 })
 
 function insertInsightsEvent (body) {
   const options = {
     uri: 'https://insights-collector.newrelic.com/v1/accounts/' + accountId + '/events',
     body,
     headers: {
       'X-Insert-Key': insertkey,
       'Content-Type': 'application/json'
     }
   }
   request.post(options, function (error, response, body) {
     if (error) throw new Error(error)
     assert.ok(response.statusCode === 200, 'New Relic Insights, Expected 200 OK response')
     const info = JSON.parse(body)
     assert.ok(info.success === true, 'Expected True results in Response Body, result was ' + info.success)
   })
 }
 
 async function call (target) {
   const options = {
     host: target,
     port: 443,
     method: 'GET',
     rejectUnauthorized: false,
     followAllRedirects: true,
     timeout: 3000
   }
 
   const req = https.request(options, function (res) {
     res.on('data', (d) => {
       // process.stdout.write(d);
     })
 
     const certificateInfo = res.socket.getPeerCertificate()
     const certExpirationDate = new Date(certificateInfo.valid_to)
     const daysToExpiration = daysBetween(currentDate, certExpirationDate)
     const certificateIssuer = certificateInfo.issuer.O
 
     const body = '[{"eventType":"SSLCertificateCheck","Url":"https://' + target + '/","Issuer":"' + certificateIssuer + '","DaysToExpiration":' + daysToExpiration + ', "ExpirationDate":' + certExpirationDate.getTime() + '}]'
     insertInsightsEvent(body)
   })
   req.on('error', function (error) {
     const message = error.message.toString().split(' ')
     switch (message[1]) {
       case 'ENOTFOUND':
         insertInsightsEvent('[{"eventType":"SSLCertificateCheck","Url":"https://' + target + '","ErrorType":"' + message[1] + '","ErrorMessage":"' + message + '"' + '}]')
         break
       case 'ETIMEDOUT':
         insertInsightsEvent('[{"eventType":"SSLCertificateCheck","Url":"https://' + target + '","ErrorType":"' + message[1] + '","ErrorMessage":"' + message + '"' + '}]')
         break
       case 'ECONNREFUSED':
         insertInsightsEvent('[{"eventType":"SSLCertificateCheck","Url":"https://' + target + '","ErrorType":"' + message[1] + '","ErrorMessage":"' + message + '"' + '}]')
         break
       case 'ECONNRESET':
         insertInsightsEvent('[{"eventType":"SSLCertificateCheck","Url":"https://' + target + '","ErrorType":"' + message[1] + '","ErrorMessage":"' + message + '"' + '}]')
         break
       case 'EPIPE':
         insertInsightsEvent('[{"eventType":"SSLCertificateCheck","Url":"https://' + target + '","ErrorType":"' + message[1] + '","ErrorMessage":"' + message + '"' + '}]')
         break
       default:
         insertInsightsEvent('[{"eventType":"SSLCertificateCheck","Url":"https://' + target + '","ErrorType":"UNKNOWN","ErrorMessage":"Truncated: ' + message.toString().substring(0, 150) + '"' + '}]')
     }
   })
   req.end()
 }
 // Do this to fake out the monitor into success
 // https://docs.newrelic.com/docs/synthetics/new-relic-synthetics/troubleshooting/monitor-produces-no-traffic
 https.get('https://' + targets[0] + '/')